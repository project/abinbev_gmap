(function ($, Drupal, once) {
  'use strict';
  Drupal.behaviors.initMap = {
    attach: function (context, settings) {

      let maps = once('map', '#map', context);

      maps.forEach(function (map) {
        let lat = $(map).data('lat')
        let lon = $(map).data('lon')
        let title = $(map).data('title')
        var myLatlng = new google.maps.LatLng(lat, lon);
        var mapOptions = {
          zoom: 8,
          center: myLatlng,
          mapTypeId: 'roadmap'
        };
        var map = new google.maps.Map(document.getElementById('map'),
            mapOptions);

        let marker = new google.maps.Marker({
          position: myLatlng,
          map,
          title: title,
        });
        let infoWindow = new google.maps.InfoWindow({
          content: '',
        });
        google.maps.event.addListener(marker, 'click', function () {
          //let user_name = drupalSettings.user_name;
          infoWindow.setContent(title);
          infoWindow.open(map, this);

        });


      });
    }
  }
})(jQuery, Drupal, once);
