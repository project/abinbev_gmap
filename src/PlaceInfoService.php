<?php

namespace Drupal\abinbev_gmap;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use \Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\Client;

/**
 * Class PlaceInfoService
 *
 * @package Drupal\abinbev_gmap\Services
 */
class PlaceInfoService {

  protected $config;

  protected $logger;

  protected $client;

  protected $database;

  protected $userDataService;

  protected $cache;

  /**
   * CustomService constructor.
   */
  public function __construct(ConfigFactory $config, LoggerChannelFactory $logger, Client $client, Connection $database, UserDataService $userDataService, CacheBackendInterface $cache) {
    $this->config = $config->get('abinbev_gmap.settings');
    $this->logger = $logger;
    $this->client = $client;
    $this->database = $database;
    $this->userDataService = $userDataService;
    $this->cache = $cache;
  }

  /**
   * @param $place
   *
   * @return mixed|\Psr\Http\Message\ResponseInterface|string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function getPlaceInfoFromGoogleByAdress($place) {
    $result = '';
    $api_key = $this->config->get('google_api_key');

    if (is_array($place)) {
      $address = $place[1] . ',' . $place[2];
    }
    else {
      $address = $place;
    }

    $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=' . $api_key;
    try {
      $result = $this->client->request('GET', $url);
      $result = json_decode($result->getBody(), TRUE);

      if (!isset($result['status'])) {
        \Drupal::logger("NOT STATUS")
          ->error('<pre>' . print_r($result, 1) . '</pre>');
      }
      if ($result['status'] == "ZERO_RESULTS") {
        //if (!empty($place[1])) {
        \Drupal::logger("GOOGLE RESULT")
          ->error('<pre>' . print_r($result, 1) . '</pre>');

        \Drupal::logger("GOOGLE RESULT PLACE")
          ->error('<pre>' . print_r($place, 1) . '</pre>');
        //}
      }
      if ($result['status'] != '"REQUEST_DENIED"') {
        return $result['results'];
      }
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return $result;
  }

  /**
   * Get Place Info.
   */
  public function getPlaceInfoFromGoogle($place_id) {
    $api_key = $this->config->get('google_api_key');

    $result = FALSE;
    $url = 'https://maps.googleapis.com/maps/api/place/details/json?key=' . $api_key . '&place_id=' . $place_id;

    try {
      $result = $this->client->request('GET', $url);
      $result = json_decode($result->getBody(), TRUE);
      if (!isset($result['status'])) {
        \Drupal::logger("NOT STATUS")
          ->error('<pre>' . print_r($result, 1) . '</pre>');
      }
      if ($result['status'] != '"REQUEST_DENIED"') {
        return $result['result'];
      }
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }

    return $result;
  }

  /**
   * @param $place_id
   * @param $data
   *
   * @return \Drupal\Core\Database\StatementInterface|int|string|null
   * @throws \Exception
   */
  function getPlaceData($place_id) {
    $connection = $this->database;
    $query = $connection->select('map_place_info', 'bi')
      ->fields('bi', [])
      ->condition('title', $place_id)
      ->execute();
    return $query->fetchAssoc();
  }

  /**
   * Get Place by ID.
   *
   * @param $place_id
   *
   * @return array
   */
  function getPlaceDataByPlaceId($place_id) {
    $connection = $this->database;
    $query = $connection->select('map_place_info', 'bi')
      ->fields('bi', [])
      ->condition('place_id', $place_id)
      ->execute();
    return $query->fetchAll();
  }

  /**
   * @param $title
   *
   * @return void
   */
  function deleteLocation($title) {
    $connection = $this->database;
    $connection->delete('map_place_info')
      ->condition('title', $title)
      ->execute();
  }

  /**
   * Insert Data to DB.
   *
   * @param $data
   */
  function saveData($place_id, $data, $isPlace = FALSE) {
    $connection = $this->database;
    if (!$isPlace) {
      $query = $connection->insert('map_place_info');
    }
    else {
      $query = $connection->update('map_place_info');
    }
    $query->fields([
      'place_id' => $place_id,
      'poc_id' => $data['poc_id'],
      'title' => $data['title'],
      'lat' => $data['lat'],
      'lon' => $data['lon'],
      'full_info' => $data['full_info'],
      'category' => $data['category'],
      'status' => $data['status'],
    ]);

    if ($isPlace) {
      if (!empty($place_id) && !empty($data['title'])) {
        $condition = $query->orConditionGroup()
          ->condition('place_id', $place_id)
          ->condition('title', $data['title']);
      }
      if (empty($place_id) && !empty($data['title'])) {
        $condition = $query->orConditionGroup()
          ->condition('title', $data['title']);
      }

      $query->condition($condition);
    }

    $r = $query->execute();
    return $r;
  }

  /**
   * Truncate All Data.
   *
   * @return int|null
   */
  function truncateData() {
    $connection = $this->database;
    $query = $connection->truncate('map_place_info')
      ->execute();
    return $query;
  }

  /**
   * @return mixed
   */
  function getAllPlaces() {
    $connection = $this->database;
    $cid = 'all_map_info';
    $data = NULL;
    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
    }
    else {

    $query = $connection->select('map_place_info', 'pi')
      ->fields('pi', [])
      ->condition('status', 'Published')
      ->execute();
    $data = $query->fetchAll();

      $this->cache->set($cid, $data);
    }
    return $data;
  }

  /**
   * @return mixed
   */
  function getSelectedPlaces($ids) {
    if (empty($ids)) {
      return [];
    }

    if (!is_array($ids)) {
      $ids = [$ids];
    }

    //$res = views_get_view_result('gmap_locations_list', 'block_1');
    $cid = 'all_map_info:' . implode(',', $ids);
    $data = NULL;
    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
    }
    else {
      $connection = $this->database;
      $query = $connection->select('map_place_info', 'pi')
        ->fields('pi', [])
        ->condition('title', $ids, 'IN')
        //->condition('status', 'Published')
        ->execute();
      $data = $query->fetchAll();

      $this->cache->set($cid, $data);
    }
    //kint($data);
    return $data;
  }

  /**
   * @param $place_id
   *
   * @return \Drupal\Core\Database\StatementInterface|int|string|null
   * @throws \Exception
   */
  function savePlace($place_id, $poc_id) {
    $this->cache->invalidate('all_map_info');
    $placeInfo = $this->getPlaceData($place_id);
    if (!$placeInfo) {
      $place = $this->getPlaceInfoFromGoogle($place_id);
      if (!empty($place['name'])) {
        $values = [
          'place_id' => $poc_id,
          'title' => $place['name'],
          'lat' => $place['geometry']['location']['lat'],
          'lon' => $place['geometry']['location']['lng'],
          'full_info' => json_encode($place),
          'status' => 'Published',
        ];
        $placeInfo = $this->saveData($place_id, $values);
      }
      else {
        \Drupal::logger('Place has no Name')->error(print_r($place_id, 1));
      }
    }
    return $placeInfo;
  }

}