<?php

namespace Drupal\abinbev_gmap\Controller;

use Drupal\abinbev_gmap\BQRequestService;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class BigQuery.
 */
class BigQueryController extends ControllerBase {

  /**
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  protected $bqService;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BQRequestService $bqService) {
    $this->config = $config_factory->get('abinbev_gmap.settings');
    $this->bqService = $bqService;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('bq_request_service')
    );
  }

  /**
   * Get Data method.
   */
  public function getData() {
    $kv_store = \Drupal::service('keyvalue.expirable')
      ->get('abinbev_gmap');
    $dataRows = $kv_store->get('bq_list_of_pocs_28,29', []);

    $content = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#title' => 'Locations',
      '#items' => $dataRows,
      '#attributes' => ['class' => 'location-list'],
      '#wrapper_attributes' => ['class' => 'container'],
      '#prefix' => '<div class="total"><h2>Total count: ' . count($dataRows) . '</h2></div>',
    ];
    return [
      $content,
    ];
  }

  /**
   * Get Data method.
   */
  public function getDataR() {
    $kv_store = \Drupal::service('keyvalue.expirable')
      ->get('abinbev_gmap');
    $dataRows = $kv_store->get('bq_list_of_pocs_30', []);

    $content = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#title' => 'Locations',
      '#items' => $dataRows,
      '#attributes' => ['class' => 'location-list'],
      '#wrapper_attributes' => ['class' => 'container'],
      '#prefix' => '<div class="total"><h2>Total count: ' . count($dataRows) . '</h2></div>',
    ];
    return [
      $content,
    ];
  }

}
