<?php

namespace Drupal\abinbev_gmap\Controller;

use Drupal\abinbev_gmap\PlaceInfoService;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormState;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class Map.
 */
class LocationsDashboardController extends ControllerBase {

  /**
   * @var \Drupal\abinbev_gmap\PlaceInfoService $placeInfoService;
   */
  private $placeInfoService;

  /**
   *   Config factory service.
   */
  public function __construct(PlaceInfoService $placeInfoService) {
    $this->placeInfoService = $placeInfoService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('place_info_service')
    );
  }

  public function showDashboard() {
    $view = views_embed_view('gmap_locations_list', 'default');
    return [
      '#markup' => \Drupal::service('renderer')->render($view),
    ];
  }

  public function editLocation($id) {
    $placeValues = $this->placeInfoService->getSelectedPlaces($id);
    $form_state = new FormState();
    $form_state->setValues($placeValues);
    $form = \Drupal::formBuilder()->getForm('Drupal\abinbev_gmap\Form\LocationEditForm', $form_state, $placeValues);
    return [
      '#theme' => 'location_edit_form',
      '#form' => $form,
    ];
  }
}
