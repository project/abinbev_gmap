<?php

namespace Drupal\abinbev_gmap\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("delete_location_button")
 */
class DeleteLocationButtonButton extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $text = [
      '#type' => 'markup',
      '#markup' => '<a href="/admin/config/abinbev_gmap/location/' . $values->map_place_info_title . '/delete_location_confirm_form" class="button button--danger button--small m-0 use-ajax" data-dialog-options="{&quot;width&quot;:600}" 
          data-dialog-type="modal" >' . t('Delete') . '</a>',
    ];

    return $text;
  }

}
