<?php

namespace Drupal\abinbev_gmap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'abinbev_gmap.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abinbev_gmap_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['events'] = [
      '#type' => 'details',
      '#title' => $this->t('Global Settings'),
      '#open' => TRUE
    ];

    $form['events']['google_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Api Key'),
      '#description' => $this->t('Google Api Key - For map render, and for PlaceID requests'),
      '#default_value' => $config->get('google_api_key') ?? '',
    ];
    $form['events']['google_default_zoom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Map - Default Map Zoom'),
      '#description' => $this->t('Google Map default zoom level on load page.'),
      '#default_value' => $config->get('google_default_zoom') ?? '16',
    ];

    $form['events']['map_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Map ID'),
      '#description' => $this->t('Google Map Id. User for set custom styles for map. For Custom settings see <a href="https://console.cloud.google.com/google/maps-apis/studio/styles/" target="_blank">Custom Map styles</a>'),
      '#default_value' => $config->get('map_id') ?? '',
    ];



    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
   $config = $this->config(static::SETTINGS);
      // Set the submitted configuration setting.
    $config->set('google_api_key', $form_state->getValue('google_api_key'));
    $config->set('google_default_zoom', $form_state->getValue('google_default_zoom'));
    $config->set('google_zoom_show_na_pocs', $form_state->getValue('google_zoom_show_na_pocs'));
    $config->set('map_id', $form_state->getValue('map_id'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   */
  public function refreshBQDataSubmit(&$form, FormStateInterface $form_state) {
    $kv_store = \Drupal::service('keyvalue.expirable')
      ->get('abinbev_gmap');
    $kv_store->setWithExpire('bq_list_of_pocs_28,29', [], 1);
    \Drupal::messenger()->addMessage('BQ cache has been cleared');
  }

}
