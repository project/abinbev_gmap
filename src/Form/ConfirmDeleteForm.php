<?php

namespace Drupal\abinbev_gmap\Form;

use Drupal\abinbev_gmap\PlaceInfoService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ModalForm class.
 */
class ConfirmDeleteForm extends FormBase {

  /**
   * @var \Drupal\abinbev_gmap\PlaceInfoService $placeInfoService ;
   */
  private $placeInfoService;

  /**
   * CustomService constructor.
   */
  public function __construct(PlaceInfoService $placeInfoService) {
    $this->placeInfoService = $placeInfoService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('place_info_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'modal_form_delete_location_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    $form = [
      'body' => [
        '#type' => 'markup',
        '#markup' => '<h2>Are you sure?</h2>',
      ],
      'id' => [
        '#type' => 'hidden',
        '#value' => $id,
      ],
      'submit' => [
        '#type' => 'submit',
        '#value' => 'Delete',
        '#ajax' => [
          'callback' => [$this, 'deleteAjaxCallback'],
          'event' => 'click',
          'url' => \Drupal\Core\Url::fromRoute('abinbev_gmap.delete_location_confirm_form', ['id' => $id]),
          'options' => [
            'query' => [
              FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
            ],
          ],
        ],
        '#attributes' => [
          'class' => [
            'button',
            'button--danger',
            'button--small',
          ],
        ],
      ],
      'cancel' => [
        '#type' => 'submit',
        '#value' => 'Cancel',
        '#ajax' => [
          'callback' => [$this, 'cancelAjaxCallback'],
          'event' => 'click',
          'url' => \Drupal\Core\Url::fromRoute('abinbev_gmap.delete_location_confirm_form', ['id' => $id]),
          'options' => [
            'query' => [
              FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
            ],
          ],
        ],
        '#attributes' => [
          'class' => [
            'button',
            'button--secondary',
            'button--small',
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'core/drupal.ajax',
          'core/drupal.dialog.ajax',
        ],
      ],
    ];


    return $form;
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $nid
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|false
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteAjaxCallback(&$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $values = $form_state->getValues();
    \Drupal::logger('abinbev_gmap')->notice(print_r($values, 1));
    $this->placeInfoService->deleteLocation($values['id']);
    \Drupal::messenger()->addMessage('Location deleted successfully');
    $response->addCommand(new RedirectCommand(Url::fromRoute('abinbev_gmap.location_dashboard')->toString()));
    //$response->addCommand(new CloseDialogCommand());
    return $response;
  }

  /**
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function cancelAjaxCallback(&$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
