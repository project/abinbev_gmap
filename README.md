# AbInbev Custom Google Map Module

Current module allows to create paragraph type Map. You can find settings for API key and for import of points on map.

# Installation guide.

1. Install module as usual.
2. Go to settings page and provide Google API key. (/admin/config/abinbev_gmap/settings)
3. Import CSV file with points. (/admin/config/abinbev_gmap/location-list). The structure of file you can
   find <a target="_blank" href="/modules/custom/abinbev_gmap/assets/files/map_import_example.csv">here</a>.
   <br /> Please use ";" as column divider.
4. Check if new paragraph type is allowed in paragraphs field in your content type.
5. Check if new paragraph type is allowed in paragraphs field in your content type.
6. Add Map paragraph to your page.
7. You can disable list of points in configs
8. You can enable categories for search in configs.
   During import you can insert categories into import CSV file. You can use multiple categories for point - use "," as
   separator.
   For filtering and using different icons - please set same category name. Category limit is 5. 
